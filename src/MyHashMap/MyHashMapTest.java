package MyHashMap;

import static org.junit.Assert.*;

/**
 * Unit tests for MyHashMap
 */
public class MyHashMapTest {

    MyHashMap<String, Integer> hashMapCustom;

    @org.junit.Before
    public void setUp() throws Exception {

       hashMapCustom = new MyHashMap<String, Integer>();

    }

    @org.junit.After
    public void tearDown() throws Exception {

        hashMapCustom = null;

    }

    @org.junit.Test
    public void get() throws Exception {

        hashMapCustom.put("a", 12);

        Integer expected = 12;
        Integer actual = hashMapCustom.get("a");
        assertEquals(expected, actual);
    }

    @org.junit.Test
    public void containsKey() throws Exception {

        hashMapCustom.put("a", 12);

        boolean expected = true;
        boolean actual = hashMapCustom.containsKey("a");
        assertEquals(expected, actual);

        expected = false;
        actual = hashMapCustom.containsKey("z");
        assertEquals(expected, actual);
    }

    @org.junit.Test
    public void isEmpty() throws Exception {

        hashMapCustom.put("a", 12);

        boolean expected = false;
        boolean actual = hashMapCustom.isEmpty();
        assertEquals(expected, actual);

        MyHashMap<String, Integer> emptyMap = new MyHashMap<String, Integer>();

        expected = true;
        actual = emptyMap.isEmpty();
        assertEquals(expected, actual);


    }

    @org.junit.Test
    public void clear() throws Exception {

        for(int i=1; i<5; ++i){
            hashMapCustom.put(String.valueOf(i), i*2);
        }

        hashMapCustom.clear();

        boolean expected = true;
        boolean actual = hashMapCustom.isEmpty();
        assertEquals(expected, actual);

    }

    @org.junit.Test
    public void size() throws Exception {

        for(int i=1; i<5; ++i){
            hashMapCustom.put(String.valueOf(i), i*2);
        }

        int expected = 4;
        int actual = hashMapCustom.size();
        assertEquals(expected, actual);

    }

    @org.junit.Test
    public void put() throws Exception {

        hashMapCustom.put("f", 200);

        boolean expectedKey =  true;
        Integer expectedValue = 200;

        boolean actualKey =  hashMapCustom.containsKey("f");
        Integer actualValue = hashMapCustom.get("f");

        assertEquals(expectedKey, actualKey);
        assertEquals(expectedValue, actualValue);

    }

    @org.junit.Test
    public void remove() throws Exception {

        hashMapCustom.put("g", 200);
        hashMapCustom.remove("g");

        boolean expected =  true;
        boolean actual = !hashMapCustom.containsKey("g") && hashMapCustom.isEmpty();

        assertEquals(expected, actual);

    }

    @org.junit.Test
    public void removeWithBucketsSizeLargerThanOne() throws Exception {

        for(int i=1; i<20; ++i){
            hashMapCustom.put(String.valueOf(i), i*2);
        }

        System.out.println(hashMapCustom);

        for(int i=1; i<20; ++i){
            hashMapCustom.remove(String.valueOf(i));
        }

        boolean expected =  true;
        boolean actual = hashMapCustom.isEmpty();

        assertEquals(expected, actual);

    }

    @org.junit.Test
    public void testToString() throws Exception {

        for(int i=1; i<20; ++i){
            hashMapCustom.put(String.valueOf(i), i*2);
        }

        String expected = "{11:22} \n" +
                "{12:24} {1:2} \n" +
                "{13:26} {2:4} \n" +
                "{14:28} {3:6} \n" +
                "{15:30} {4:8} \n" +
                "{16:32} {5:10} \n" +
                "{17:34} {6:12} \n" +
                "{18:36} {7:14} \n" +
                "{19:38} {8:16} \n" +
                "{9:18} \n" +
                "{10:20} \n";

        String actual = hashMapCustom.toString();

        assertEquals(expected, actual);

    }

}