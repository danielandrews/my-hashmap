package MyHashMap;

/**
 * Created by danielandrews on 2016-10-25.
 * <p>
 * Simple Implementation of a Hash Map not requiring built in classes
 * Providing O(1) complexity for many operations, namely get and put
 * **************************************************************************
 * <p>
 * Description: Simple HashMap Implementation
 * <p>
 */
public class MyHashMap<K,V> {

    private static final int DEFAULT_INITIAL_CAPACITY = 16;
    private int numberOfEntries;
    private static int tableSize = DEFAULT_INITIAL_CAPACITY;

    private Entry[] table;

    private class Entry<K,V> {

        private final K key;
        private V value;
        Entry<K,V> next;


        public Entry(K key, V value, Entry<K,V> next){
            this.key = key;
            this.value = value;
            this.next = next;
        }


        public V getValue(){
            return value;
        }

        public void setValue(V value){
            this.value = value;
        }

        public K getKey(){
            return key;
        }
    }

    @SuppressWarnings("unchecked")
    public MyHashMap(){
        table = new Entry[DEFAULT_INITIAL_CAPACITY];
    }

    private static final int getBetterHashCode(Object key){
        int h;
        return (key == null) ? 0: (h = key.hashCode()) ^ (h >>> 16);

    }

    private static final int getBucketIndex(int hash){
        return hash & (tableSize - 1);
    }

    public boolean containsKey(K key){

        return (get(key) != null);

    }

    public boolean isEmpty(){

        return numberOfEntries==0;
    }


    @SuppressWarnings("unchecked")
    public V get(K key){

        int hash =  getBetterHashCode(key);

        int bucket = getBucketIndex(hash);

        Entry<K,V> element = table[bucket];

        while(element != null){

            if(element.getKey().equals(key)){
                return element.getValue();
            }

            element = element.next;

        }

        return null;

    }

    @SuppressWarnings("unchecked")
    public void put(K key, V value){

        int hash =  getBetterHashCode(key);

        int bucket = getBucketIndex(hash);

        Entry<K,V> entry = table[bucket];

        while(entry != null){

            if(entry.getKey().equals(key)){
                entry.setValue(value);
                return;
            }
            entry = entry.next;
        }

        Entry<K,V> newBucketEntry = new Entry<K, V>(key, value, table[bucket]);
        table[bucket] = newBucketEntry;
        numberOfEntries++;

    }

    public void clear(){

        for(int i=0; i<table.length; ++i){
            table[i] = null;
        }

        numberOfEntries = 0;
    }

    @SuppressWarnings("unchecked")
    @Override
    public String toString(){

        StringBuilder stringBuilder = new StringBuilder();

        for(Entry<K, V> entry: table){
            while(entry!=null){
                stringBuilder.append("{")
                        .append(entry.getKey())
                        .append(":")
                        .append(entry.getValue())
                        .append("} ");
                entry=entry.next;
                if(entry == null) {
                    stringBuilder.append(System.getProperty("line.separator"));
                }
            }
        }

        return stringBuilder.toString();

    }


    public int size(){
        return numberOfEntries;
    }

    @SuppressWarnings("unchecked")
    public void remove(K key){

        int hash =  getBetterHashCode(key);

        int bucket = getBucketIndex(hash);

        Entry<K,V> entry = table[bucket];
        Entry<K,V> previousEntry = entry;

        while(entry != null){

            Entry<K,V>  nextEntry = entry.next;

            if(entry.getKey().equals(key)){

                numberOfEntries--;

                if(previousEntry == entry){
                    table[bucket] = nextEntry;
                }
                else {
                    previousEntry.next = nextEntry;
                }
            }
            previousEntry=entry;
            entry = entry.next;
        }
    }


}
